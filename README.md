# jfinal_fk

#### 项目介绍
在jfinal的基础上，自动生成级连属性，
如

1:N  主表 material.id  分录表 materialpart.parentid 

1:1  主表 material.id  扩展表 materialinfo.materialid  


会自动生成级连的下级属性 ， 如：
 

public abstract class BaseMaterial<M extends BaseMaterial<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}
	
	
	public List<Materialpart> getParts(String fields){
		if(_getAttrs().containsKey("parts")){
			return get("parts"); 
		}else{
			List<Materialpart> val = new Materialpart().dao().find("select " + fields + " from materialpart where parentid = ? ", get("id"));
			_getAttrs().put("parts", val);
			return  val;
		}
	}
	
	public List<Materialpart> getParts(){
		return getParts("*");
	}
	
			
			
	public Materialinfo getMaterialinfo(String fields){
		if(_getAttrs().containsKey("materialinfo")){
			return get("materialinfo"); 
		}else{
			Materialinfo val = new Materialinfo().dao().findFirst("select " + fields + " from materialinfo where materialid = ? ", get("id"));
			_getAttrs().put("materialinfo", val);
			return  val;
		}
	}
	
	public Materialinfo getMaterialinfo(){
		return getMaterialinfo("*");
	}
	

	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}
	
	

}



public abstract class BaseMaterialinfo<M extends BaseMaterialinfo<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}
	
	

	public void setMaterialid(java.lang.Integer materialid) {
		set("materialid", materialid);
	}
	
	public java.lang.Integer getMaterialid() {
		return getInt("materialid");
	}
	
    public Material getMaterial(String fields){
    	if(_getAttrs().containsKey("material")){
			return get("material"); 
		}else{
			Material val = new Material().dao().findFirst("select " + fields + " from material where  id = ? limit 1 ", get("materialid"));
			_getAttrs().put("material", val);
			return  val;
		} 
  	}
  	
  	public Material getMaterial(){
  		return getMaterial("*");
  	}
	

	public void setQuantiy(java.lang.Integer quantiy) {
		set("quantiy", quantiy);
	}
	
	public java.lang.Integer getQuantiy() {
		return getInt("quantiy");
	}
	
	

}


修改说明:
1._JFinalDemoGenerator 中 GeneratorEx generator = new GeneratorEx，然后按格式配置外键关系setFK

2.扩展类  ColumnMetaEx， MetaBuilderEx， BaseModelGeneratorEx ，修改模版 base_model_template_fk.jf

3.更深入的 应用 ，如save，update，delete时自动更新从表，需要波总统一考量框架的适用性
 