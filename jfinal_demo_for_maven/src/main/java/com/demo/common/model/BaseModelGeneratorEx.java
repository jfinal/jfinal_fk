package com.demo.common.model;

import java.util.List;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.generator.BaseModelGenerator;
import com.jfinal.plugin.activerecord.generator.TableMeta;

public class BaseModelGeneratorEx extends BaseModelGenerator {

	protected String modelPackageName;
	
	public BaseModelGeneratorEx(String baseModelPackageName, String baseModelOutputDir, String modelPackageName) {
		super(baseModelPackageName, baseModelOutputDir);
		this.modelPackageName = modelPackageName;
	}

	public void generate(List<TableMeta> tableMetas, FKEngine fkEngine) {
		System.out.println("Generate base model with FK ...");
		System.out.println("Base Model Output Dir: " + baseModelOutputDir);
		
		for (TableMeta tableMeta : tableMetas) {
			genBaseModelContent(tableMeta, fkEngine);
		}
		writeToFile(tableMetas); 
	}
	
	
	protected void genBaseModelContent(TableMeta tableMeta, FKEngine fkEngine) {
		Kv data = Kv.by("baseModelPackageName", baseModelPackageName);
		data.set("generateChainSetter", generateChainSetter);
		data.set("tableMeta", tableMeta); 
		data.set("modelPackageName", modelPackageName); 
		tableMeta.baseModelContent = engine.getTemplate("/com/demo/common/model/base_model_template_fk.jf").renderToString(data);
	}

}
