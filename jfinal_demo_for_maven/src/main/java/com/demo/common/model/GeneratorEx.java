package com.demo.common.model;

import java.util.List;

import javax.sql.DataSource;

import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.activerecord.generator.TableMeta;

public class GeneratorEx extends Generator {

	protected FKEngine fkEngine;
	protected BaseModelGeneratorEx baseModelGeneratorEx; 
	protected MetaBuilderEx metaBuilderEx;

	public GeneratorEx(DataSource dataSource, String baseModelPackageName, String baseModelOutputDir,
			String modelPackageName, String modelOutputDir) {
		super(dataSource, baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
		fkEngine = new FKEngine();
		this.metaBuilderEx = new MetaBuilderEx(dataSource);
		baseModelGeneratorEx = new BaseModelGeneratorEx(baseModelPackageName, baseModelOutputDir, modelPackageName); 
	}

	

	public void setFK(String expr) { 
		fkEngine.addFK(expr);
	}
	
	
	public void generate(){
		
		if (dialect != null) {
			metaBuilderEx.setDialect(dialect);
		}
		
		long start = System.currentTimeMillis();
		List<TableMeta> tableMetas = metaBuilderEx.build(fkEngine);
		if (tableMetas.size() == 0) {
			System.out.println("TableMeta 数量为 0，不生成任何文件");
			return ;
		}
		
		//改动
		baseModelGeneratorEx.generate(tableMetas,fkEngine);
		 
		if (modelGenerator != null) {
			modelGenerator.generate(tableMetas);
		} 
		
		mappingKitGenerator.generate(tableMetas);
		
		if (dataDictionaryGenerator != null && generateDataDictionary) {
			dataDictionaryGenerator.generate(tableMetas);
		}
		
		long usedTime = (System.currentTimeMillis() - start) / 1000;
		System.out.println("Generate complete in " + usedTime + " seconds.");
		
	}

}
